package INF101.lab2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    public final int MAX_SIZE = 20;
    private final ArrayList<FridgeItem> fridge;

    Fridge() {
        this.fridge = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return fridge.size();
    }

    @Override
    public int totalSize() {
        return MAX_SIZE;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(fridge.size() < MAX_SIZE){
            fridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(fridge.contains(item)){
            fridge.remove(item);
        }
        else {
            throw new NoSuchElementException("Item is not in fridge");
        }
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expired = new ArrayList<>();

        // Måtte ha Iterator for å unngå ConcurrentModificationException (https://www.youtube.com/watch?v=83Omy_C8t24&ab_channel=learnprogramingbyluckysir)
        Iterator<FridgeItem> iterator = fridge.iterator();
        while(iterator.hasNext()){
            FridgeItem item = iterator.next();
            if(item.hasExpired()){
                expired.add(item);
                iterator.remove();
            }
        }

        return expired;
    }
}